import json
from datetime import datetime

import requests


def login() -> requests.Session:
    form_data = {
        "username": "trino",
        "password": "",
        "redirectPath": ""
    }
    session = requests.Session()
    session.post(url="http://localhost:8080/ui/login", data=form_data)
    return session


def collect_metrics(sess: requests.Session, query_id: str):
    json_response = sess.get(url=f"http://localhost:8080/ui/api/query/{query_id}?pretty")
    query_metrics: dict = json_response.json()
    with open(f"log/{datetime.now()}.json", "w") as f:
        json.dump(query_metrics, f, indent=4)


if __name__ == '__main__':
    s = login()
    id_ = "20220617_193428_00086_yi4fi"
    collect_metrics(s, id_)
