import json
from pathlib import Path


def merge_json(path: Path):
    res = []
    for file in path.glob("*.json"):

        if "merged" not in str(file):
            with open(file, "r") as f:
                j = json.load(f)
                # Flatten query stats
                query_stats: dict = j.pop("queryStats")
                for x in query_stats.keys():
                    j[f"queryStats_{x}"] = query_stats[x]

                res.append(j)
    with open(path / "merged.json", "w") as f:
        json.dump(res, f, indent=4)


if __name__ == '__main__':
    # Prepare
    p = Path("./log")
    merge_json(p)
    # Analyze
