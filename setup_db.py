from typing import List, Tuple

import trino.dbapi
from trino.dbapi import connect

from collect_query_metrics import login, collect_metrics

host = "localhost"
port = 8080
user = "trino"
catalog = "test"
schema = "myschema"

create_schema_sql = \
    """
        CREATE SCHEMA IF NOT EXISTS delta.myschema
        WITH (location='s3a://test/myschema')
    """

create_table_sql = \
    """
        CREATE TABLE IF NOT EXISTS delta.myschema.test (id INTEGER, totalprice double)
    """


def create_schema(cursor: trino.dbapi.Cursor) -> List[bool]:
    cursor.execute(create_schema_sql)
    r = cursor.fetchone()
    return r


def create_table(cursor: trino.dbapi.Cursor) -> List[bool]:
    cursor.execute(create_table_sql)
    r = cursor.fetchone()
    return r


def insert(cursor: trino.dbapi.Cursor) -> List[List]:
    for x in range(0, 10):
        test_id = x
        price = 10 * x
        insert_data_sql = \
            f"INSERT INTO delta.myschema.test (id, totalprice) VALUES ({test_id},{price})"
        cursor.execute(insert_data_sql)
        return cursor.fetchall()


def fetch_all(cursor: trino.dbapi.Cursor) -> Tuple[List[List], str]:
    cursor.execute(
        """
        SELECT * FROM delta.myschema.test
        """
    )
    res = cursor.fetchall()
    query_id = cursor.stats["queryId"]
    return res, query_id


def analyze(cursor: trino.dbapi.Cursor) -> Tuple[List[List], str]:
    cursor.execute(
        """
        SELECT sum( totalprice ) FROM delta.myschema.test
        """
    )
    res = cursor.fetchall()
    query_id = cursor.stats["queryId"]
    return res, query_id


def setup() -> trino.dbapi.Connection:
    connection = connect(
        host=host,
        port=port,
        user=user,
        catalog=catalog,
        schema=schema,
    )
    cursor: trino.dbapi.Cursor = connection.cursor()
    create_schema(cursor)
    create_table(cursor)
    insert(cursor)
    return connection


if __name__ == "__main__":
    conn = setup()
    sess = login()

    cur = conn.cursor()
    _, id_ = fetch_all(cur)
    collect_metrics(sess, id_)
    _, id_ = analyze(cur)
    collect_metrics(sess, id_)
